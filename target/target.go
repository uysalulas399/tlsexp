package target

import "time"

// Entry holds a requested entry
type Entry struct {
	Requested   string
	Expirations []Expiration
	Protocol    string
	Port        string
}

// Expiration holds an expiration date for a specific entry
type Expiration struct {
	Hostname string
	Expires  time.Time
	Error    error
}

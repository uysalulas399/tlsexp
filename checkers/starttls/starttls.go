package starttls

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
	"sync"
	"time"

	"github.com/Privowny/tlsexp/target"
	log "github.com/sirupsen/logrus"
)

// Checker type handles TLS checking
type Checker struct {
	Hostname string
	Protocol string
	Port     string
}

// Check gets expiration dates for starttls smtp servers
func (c *Checker) Check(e chan target.Entry, wg *sync.WaitGroup) {
	defer wg.Done()

	tg := target.Entry{
		Requested: c.Hostname,
		Protocol:  c.Protocol,
		Port:      c.Port,
	}

	tg.Port = "25"
	if c.Protocol == "submission" {
		tg.Port = "587"
	}

	tg.Expirations = append(tg.Expirations, target.Expiration{Hostname: c.Hostname})

	for i, hst := range tg.Expirations {
		t, err := getNotAfter(hst.Hostname, tg.Port)
		if err != nil {
			tg.Expirations[i].Error = err
			continue
		}
		tg.Expirations[i].Expires = t
	}

	e <- tg
}

func getNotAfter(srv, port string) (time.Time, error) {
	full := srv + ":" + port
	log.Debugf("checking starttls on %s", full)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         srv,
	}

	c, err := smtp.Dial(full)
	if err != nil {
		return time.Unix(0, 0), err
	}

	defer c.Close()

	c.StartTLS(tlsconfig)
	st, ok := c.TLSConnectionState()

	if !ok {
		return time.Unix(0, 0), fmt.Errorf("unable to get TLSConnectionState")
	}

	// Set expiration in 20 years to start
	nextExp := time.Now().Add(time.Hour * 24 * 365 * 20)

	for _, cert := range st.PeerCertificates {
		if cert.NotAfter.Before(nextExp) {
			nextExp = cert.NotAfter
		}
		// fmt.Printf("PeerCertificate %s\n", cert.Subject.CommonName)
		// fmt.Printf("NotBefore %s\n", cert.NotBefore)
		// fmt.Printf("NotAfter %s\n", cert.NotAfter)
	}

	return nextExp, nil
}

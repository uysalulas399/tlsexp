package generic

import (
	"crypto/tls"
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/Privowny/tlsexp/target"
	"github.com/pkg/errors"

	log "github.com/sirupsen/logrus"
)

// Checker type handles TLS checking
type Checker struct {
	Hostname string
	Protocol string
	Port     string
}

// Services contains a map of supported generic services
var Services = map[string]string{
	"https":       "443",
	"http":        "443", // convenience helper
	"smtps":       "465",
	"submissions": "465",
	"imaps":       "993",
	"pop3s":       "995",
}

// Check gets expiration dates for starttls smtp servers
func (c *Checker) Check(e chan target.Entry, wg *sync.WaitGroup) {
	defer wg.Done()

	tg := target.Entry{
		Requested: c.Hostname,
		Protocol:  c.Protocol,
		Port:      c.Port,
	}
	entries, err := net.LookupHost(c.Hostname)

	if err != nil {
		tg.Expirations = append(tg.Expirations, target.Expiration{
			Hostname: c.Hostname,
			Error:    errors.Wrap(err, "unable to lookup host"),
		})
	}

	for _, m := range entries {
		if strings.Contains(m, ":") {
			m = fmt.Sprintf("[%s]", m)
		}
		tg.Expirations = append(tg.Expirations, target.Expiration{Hostname: m})
	}

	for i, hst := range tg.Expirations {
		t, err := getNotAfter(hst.Hostname, c.Port)
		if err != nil {
			log.Errorf("error checking %s: %v", c.Hostname, err)
			tg.Expirations[i].Error = err
			continue
		}
		tg.Expirations[i].Expires = t
	}

	e <- tg
}

func getNotAfter(srv string, port string) (time.Time, error) {
	full := fmt.Sprintf("%s:%s", srv, port)
	log.Debugf("checking generic TLS service on %s", full)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         srv,
	}

	c, err := net.DialTimeout("tcp", full, 10*time.Second)
	if err != nil {
		return time.Unix(0, 0), err
	}

	defer c.Close()

	tlsConn := tls.Client(c, tlsconfig)
	defer tlsConn.Close()

	err = tlsConn.Handshake()
	if err != nil {
		return time.Unix(0, 0), fmt.Errorf("unable to TLS handshake with host %s: %v", full, err)
	}

	st := tlsConn.ConnectionState()

	// Set expiration in 20 years to start
	nextExp := time.Now().Add(time.Hour * 24 * 365 * 20)

	for _, cert := range st.PeerCertificates {
		if cert.NotAfter.Before(nextExp) {
			nextExp = cert.NotAfter
		}
	}

	return nextExp, nil
}

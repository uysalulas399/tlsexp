module github.com/Privowny/tlsexp

go 1.13

require (
	github.com/namsral/flag v1.7.4-pre
	github.com/olekukonko/tablewriter v0.0.5-0.20200416053754-163badb3bac6
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.3.0
	github.com/prometheus/common v0.9.1 // indirect
	github.com/sirupsen/logrus v1.4.2
)

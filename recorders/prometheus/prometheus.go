package prometheus

import (
	"fmt"
	"net/http"
	"time"

	"github.com/Privowny/tlsexp/target"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	log "github.com/sirupsen/logrus"
)

// Config for prometheus exported
type Config struct {
	Bind             string
	Port             int
	CheckRequests    *prometheus.Counter
	SubcheckRequests *prometheus.Counter
	SubcheckErrors   *prometheus.Counter
	RemainingHours   *prometheus.GaugeVec
}

// Init the exporter
func (c *Config) Init() {
	// If no prom port or "run once" mode, no need to start prom listener
	if c.Port == 0 {
		log.Debug("prometheus port set to 0, skippinig init")
		return
	}

	cr := prometheus.NewCounter(prometheus.CounterOpts{
		Name: "check_total",
		Help: "The total number of checks executed",
	})
	c.CheckRequests = &cr

	sr := prometheus.NewCounter(prometheus.CounterOpts{
		Name: "check_tls_requests_total",
		Help: "The total number of TLS checks executed",
	})
	c.SubcheckRequests = &sr

	se := prometheus.NewCounter(prometheus.CounterOpts{
		Name: "check_errors_total",
		Help: "The total number of TLS check errors",
	})
	c.SubcheckErrors = &se

	c.RemainingHours = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "tls_remaining_hours",
		Help: "Number of remaining hours for certificate",
	},
		[]string{"requested_hostname", "tested_hostname", "protocol"},
	)

	prometheus.MustRegister(*c.CheckRequests)
	prometheus.MustRegister(*c.SubcheckRequests)
	prometheus.MustRegister(*c.SubcheckErrors)
	prometheus.MustRegister(*c.RemainingHours)

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(fmt.Sprintf("%s:%d", c.Bind, c.Port), nil)
	log.Debugf("prometheus initialized and listening at %s:%d", c.Bind, c.Port)
}

// Finalize printing
func (c *Config) Finalize() {

}

// Log point to TSDB
func (c *Config) Log(requested, protocol, port string, e target.Expiration) {
	// Skip if port is not set
	if c.Port == 0 {
		return
	}

	if e.Error != nil {
		log.Errorf("unable to check %s: %v\n", e.Hostname, e.Error)
		(*c.SubcheckErrors).Inc()
	} else {
		// fmt.Printf("%s (%s://%s) expires %s\n", e.Hostname, protocol, requested, e.Expires)
		(*c.SubcheckRequests).Inc()
		c.RemainingHours.With(prometheus.Labels{
			"requested_hostname": requested,
			"tested_hostname":    e.Hostname,
			"protocol":           protocol,
		}).Set(float64(e.Expires.Sub(time.Now()) / time.Hour))
	}
}

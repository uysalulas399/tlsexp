package human

import (
	"fmt"
	"os"
	"sort"
	"text/tabwriter"

	// "text/tabwriter"
	"time"

	"github.com/Privowny/tlsexp/target"
	"github.com/olekukonko/tablewriter"

	log "github.com/sirupsen/logrus"
)

// Config is a void struct here
type Config struct {
	output [][]string
}

var (
	info     = teal
	warn     = yellow
	critical = red
)

var (
	black   = color("\033[1;30m%s\033[0m")
	red     = color("\033[1;31m%s\033[0m")
	green   = color("\033[1;32m%s\033[0m")
	yellow  = color("\033[1;33m%s\033[0m")
	purple  = color("\033[1;34m%s\033[0m")
	magenta = color("\033[1;35m%s\033[0m")
	teal    = color("\033[1;36m%s\033[0m")
	white   = color("\033[1;37m%s\033[0m")
)

func color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString,
			fmt.Sprint(args...))
	}
	return sprint
}

// Init is fake here since we need no init
func (s *Config) Init() {
	log.Debug("human output initialized")
}

// Finalize2 printing
func (s *Config) Finalize2() {
	w := new(tabwriter.Writer)
	// w.Init(os.Stdout, 0, 0, 0, ' ', 0)
	w.Init(os.Stdout, 8, 0, 2, ' ', 0)

	fmt.Fprintln(w, "URL\taddress\tport\tproto\texpires (UTC)\thours\t")

	for _, o := range s.output {
		fmt.Fprintln(w, o)
	}
	w.Flush()
}

// Finalize printing
func (s *Config) Finalize() {
	// s.Finalize2()

	// s.sortByHours()
	sort.Slice(s.output, func(i, j int) bool {
		return s.output[i][4] < s.output[j][4]
	})

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"URL", "address", "port", "proto", "expires", "hours"})
	table.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.SetCenterSeparator("|")
	table.SetColumnAlignment([]int{
		tablewriter.ALIGN_LEFT, tablewriter.ALIGN_CENTER, tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_LEFT, tablewriter.ALIGN_LEFT, tablewriter.ALIGN_RIGHT,
	})
	table.SetAutoWrapText(false)

	table.SetAutoMergeCellsByColumnIndex([]int{0})
	table.SetRowLine(true)

	table.AppendBulk(s.output) // Add Bulk Data
	table.Render()

	// for _, o := range s.output {
	// 	fmt.Println(o)
	// }
}

// Log a point to stderr
func (s *Config) Log(requested, protocol, port string, e target.Expiration) {
	left := float64(e.Expires.Sub(time.Now()) / time.Hour)
	col := teal

	switch {
	case left < 120:
		col = critical
	case left < 240:
		col = warn
	}

	s.output = append(s.output, []string{
		col(requested),
		col(e.Hostname),
		col(port),
		col(protocol),
		col(e.Expires.Format("2006-01-02 15:04")),
		col(fmt.Sprintf("%.2f", left)),
	})
}

// func (s *Config) sortByHours() {
// 	sorted := [][]string{}
// 	for _, entry := range s.output {

// 	}
// }

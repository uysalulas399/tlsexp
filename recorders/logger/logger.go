package logger

import (
	"time"

	"github.com/Privowny/tlsexp/target"

	log "github.com/sirupsen/logrus"
)

// Config is a void struct here
type Config struct{}

// Init is fake here since we need no init
func (s *Config) Init() {
	log.Debug("logger initialized")
}

// Finalize log printing
func (s *Config) Finalize() {

}

// Log a point to stderr
func (s *Config) Log(requested, protocol, port string, e target.Expiration) {
	log.WithFields(log.Fields{
		"requested":  requested,
		"hostname":   e.Hostname,
		"port":       port,
		"protocol":   protocol,
		"expires":    e.Expires,
		"hours_left": float64(e.Expires.Sub(time.Now()) / time.Hour),
	}).Infof("expiration")
}
